# DirectMairie

## Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with directmairie](#setup)
    * [What directmairie affects](#what-directmairie-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with directmairie](#beginning-with-directmairie)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module installs and configures the application DirectMairie.

## Setup

### What directmairie affects 

This module does following things :

* installs of Java.
* installs and configuration of Postgres, creates of role (aka user) and database.
* installs and configuration of DirectMairie app.

### Setup Requirements 

* An OS account owner of DirectMairie files.
* A SMTP relay with an user account and password.

### Beginning with directmairie

```
  class { 'directmairie':
    host             => 'directmairie.example.org',
    smtp_host        => 'smtp.example.com',
    smtp_port        => 465,
    smtp_user_name   => 'smtpuser',
    smtp_user_passwd => 'smtppassword',
    app_dir          => '/opt/directmairie',
    picture_dir      => '/opt/directmairie_img',
    security_key     => 'iUJ4Oh5DQubbspfSg8RKVFsqnvbbxc64ZICUFG1z2R4=',
    db_user_password => 'dbpassword',
  }
```

## Usage

Include usage examples for common use cases in the **Usage** section. Show your users how to use your module to solve problems, and be sure to include code examples. Include three to five examples of the most important or common tasks a user can accomplish with your module. Show users how to accomplish more complex tasks that involve different types, classes, and functions working in tandem.

## Reference

Details in `REFERENCE.md` file.

## Limitations

Details in `metadata.json` file.

## Development

Home at URL https://gitlab.adullact.net/directmairie/puppet-directmairie

Issues and MR are welcome.

## Release Notes/Contributors/Etc.

```
Copyright (C) 2018 Association des Développeurs et Utilisateurs de Logiciels Libres
                     pour les Administrations et Colléctivités Territoriales.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/agpl.html>.

```

