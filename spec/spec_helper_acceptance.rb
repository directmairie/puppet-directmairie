require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper
install_module_on(hosts)
install_module_dependencies_on(hosts)

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    # creates some users. They will be used by directmairie
    pp_prepare_sut = %(
      user { 'directmairie': ensure => present }
    )
    apply_manifest(pp_prepare_sut, catch_failures: true)
  end
end
