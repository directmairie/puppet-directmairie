require 'spec_helper_acceptance'

app_dir = '/opt/directmairie_app'

describe 'directmairie' do
  context 'with defaults' do
    pp = %(
      class { 'directmairie':
        app_dir          => '#{app_dir}',
        picture_dir      => '/opt/directmairie_img',
        security_key     => 'iUJ4Oh5DQubbspfSg8RKVFsqnvbbxc64ZICUFG1z2R4=',
        db_user_password => 'dbpassword',
        checksum_verify  => true,
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      shell('sleep 10') # java needs some time
      apply_manifest(pp, catch_changes: true)
    end

    describe port(5432) do
      it { is_expected.to be_listening.on('127.0.0.1').with('tcp') }
    end

    describe process('java') do
      it { is_expected.to be_running }
    end
    describe port(8080) do
      it { is_expected.to be_listening }
    end

    describe file(app_dir.to_s) do
      it { is_expected.to be_directory }
    end
    describe file('/etc/systemd/system/directmairie.service') do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 644 }
      it { is_expected.to be_owned_by 'root' }
      its(:content) { is_expected.to match %r{.*--directmairie\.hosting\.host="directmairie.example.org".*} }
    end
  end

  context 'with systemd unit update' do
    pp = %(
      class { 'directmairie':
        host             => 'directmairie.example.com',
        smtp_host        => 'smtp.example.com',
        smtp_port        => 25,
        smtp_user_name   => 'smtpuser_new',
        smtp_user_passwd => 'smtppassword',
        app_dir          => '#{app_dir}',
        picture_dir      => '/opt/directmairie_img',
        security_key     => 'iUJ4Oh5DQubbspfSg8RKVFsqnvbbxc64ZICUFG1z2R4=',
        db_user_password => 'dbpassword',
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      shell('sleep 10') # java needs some time
      apply_manifest(pp, catch_changes: true)
    end

    describe port(5432) do
      it { is_expected.to be_listening.on('127.0.0.1').with('tcp') }
    end

    describe file('/etc/systemd/system/directmairie.service') do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 644 }
      it { is_expected.to be_owned_by 'root' }
      its(:content) { is_expected.to match %r{.*--directmairie\.hosting\.host="directmairie.example.com".*} }
    end
    describe process('java') do
      it { is_expected.to be_running }
      its(:args) { is_expected.to match %r{directmairie\.mail\.lost-password\.from=noreply@exemple\.com.*spring\.mail\.username=smtpuser_new} }
    end
    describe port(8080) do
      it { is_expected.to be_listening }
    end
  end

  context 'with SMTP and TLS enabled' do
    pp = %(
      class { 'directmairie':
        app_dir          => '#{app_dir}',
        picture_dir      => '/opt/directmairie_img',
        security_key     => 'iUJ4Oh5DQubbspfSg8RKVFsqnvbbxc64ZICUFG1z2R4=',
        db_user_password => 'dbpassword',
        smtp_enabletls   => true,
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      shell('sleep 10') # java needs some time
      apply_manifest(pp, catch_changes: true)
    end

    describe port(5432) do
      it { is_expected.to be_listening.on('127.0.0.1').with('tcp') }
    end

    describe process('java') do
      it { is_expected.to be_running }
      its(:args) { is_expected.to match %r{directmairie\.mail\.lost-password\.from=noreply@exemple\.com} }
    end
    describe port(8080) do
      it { is_expected.to be_listening }
    end

    describe file('/etc/systemd/system/directmairie.service') do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 644 }
      it { is_expected.to be_owned_by 'root' }
      its(:content) { is_expected.to match %r{.*--spring\.mail\.smtp\.starttls\.enable=false.*} }
    end
  end
end
