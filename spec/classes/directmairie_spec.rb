require 'spec_helper'

describe 'directmairie' do
  let(:default_params) do
    {
      app_dir: '/home/directmairie/DirectMairie',
      picture_dir: '/home/directmairie/DirectMairie-pictures',
      security_key: '0123654789abcdefghijklm',
      db_user_password: 'S3cr#TPassw0rdz',
      java_package: 'openjdk-foo',
    }
  end
  let(:pre_condition) do
    'require postgresql::server'
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(service_provider: 'systemd') }
      let(:params) { default_params }

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_package('openjdk-foo').that_notifies('Service[directmairie]') }
      it { is_expected.to contain_archive('download-directmairie-jar').that_notifies('Service[directmairie]') }
      it { is_expected.to contain_archive('download-directmairie-jar').with('ensure' => 'present', 'checksum_verify' => false) }

      context 'with checksum verified' do
        let(:params) do
          default_params.merge(
            checksum_verify: true,
          )
        end

        it { is_expected.to contain_archive('download-directmairie-jar').with('ensure' => 'present', 'checksum_verify' => true) }
      end
    end
  end
end
