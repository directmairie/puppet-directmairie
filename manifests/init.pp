# @summary Install and configure DirectMairie
#
# Install and configure DirectMairie, the open source citizen participation tool for cities streets
#
# @example
#     class { 'directmairie':
#       host             => 'directmairie.example.org',
#       smtp_host        => 'smtp.example.org',
#       smtp_port        => 587,
#       smtp_user_name   => 'kate@example.org',
#       smtp_user_passwd => 'RunningUpThatHill!',
#       app_dir          => '/home/directmairie/DirectMairie',
#       picture_dir      => '/home/directmairie/DirectMairie-pictures',
#       security_key     => '0123654789abcdefghijklm',
#       db_user_password => 'S3cr#TPassw0rdz',
#     }
#
# @param db_host Hostname to reach database server
# @param db_port Port to reach database server
# @param db_name Name of the database to use
# @param db_user_name Username to connect to the database to use
# @param system_username System username holding the application
# @param host Hostname of DirectMairie webapp
# @param smtp_host Hostname of SMTP relay host
# @param smtp_port Port of SMTP relay host
# @param smtp_user_name Username for SMTP credentials
# @param smtp_user_passwd Password for SMTP credentials
# @param smtp_enabletls
#   By default SpringBoot use STARTTLS. RFC8314 recommands to prefer TLS.
#   False : Connect SMTP service with STARTTLS. True : Connext SMTP service with TLS
# @param app_ensure Define status of systemd unit for DirectMairie
# @param app_dir Directory in which application will be copied
# @param picture_dir Directory holding user-uploaded pictures (may be fat)
# @param security_key Secret key for the application (used as salt)
# @param db_user_password Password for DirectMairie database
# @param java_package Java package to be installed (default value stored in Hiera)
# @param download_url URL where DirectMairie jar file is stored. The current default value donwload DirectMairie 2.1.0-alpha.2.
# @param download_checksum Archive file checksum (match checksum_type).
# @param checksum_verify True enable the optional checksum verification of archive files.
# @param checksum_type Archive file checksum type.
# @param mailfrom The email address used as the sender address of all emails sent to users
#
class directmairie (
  String $app_dir,
  String $picture_dir,
  String $security_key,
  String $db_user_password,
  Stdlib::Fqdn $host = 'directmairie.example.org',
  Stdlib::Host $smtp_host = '127.0.0.1',
  String[1] $smtp_user_name = 'changeme',
  String[1] $smtp_user_passwd = 'changeme',
  Integer $smtp_port = 25,
  Boolean $smtp_enabletls = false,
  String $java_package = 'openjdk-11-jdk',
  Enum['running','stopped'] $app_ensure = 'running',
  Stdlib::HTTPUrl $download_url = 'https://gitlab.adullact.net/directmairie/directmairie/-/package_files/99/download',
  String $download_checksum = '90f615501cf530dd709994b5913efdae83cccdaa5b907dd8be8f204be7c5a288',
  Boolean $checksum_verify = false,
  Enum['md5', 'sha1', 'sha2','sha256', 'sha384', 'sha512'] $checksum_type = 'sha256',
  String $db_host           = 'localhost',
  Integer $db_port          = 5432,
  String $db_name           = 'dm_dbname',
  String $db_user_name      = 'dm_dbuser',
  String $system_username   = 'directmairie',
  Stdlib::Email $mailfrom   = 'noreply@exemple.com',
) {
  # Filename of DirectMairie jar file (build artefact)
  $_jar_name = 'directmairie.jar'

  $_systemd_unit_file = '/etc/systemd/system/directmairie.service'

  # JDBC URL string
  $_directmairie_jdbc_url = "jdbc:postgresql://${db_host}:${db_port}/${db_name}"

  # PREREQUISITES

  package { $java_package:
    ensure => present,
    notify => Service['directmairie'],
  }

  # install postgresql with defaults
  include postgresql::server

  # DB + Postgres user creation
  postgresql::server::db { $db_name:
    user     => $db_user_name,
    password => postgresql::postgresql_password($db_user_name, $db_user_password),
  }

  # PREPARE DIRECTMAIRIE

  # Get DirectMairie jar file
  archive { 'download-directmairie-jar':
    ensure          => 'present',
    path            => "${directmairie::app_dir}/${_jar_name}",
    source          => $directmairie::download_url,
    checksum_verify => $directmairie::checksum_verify,
    checksum_type   => $directmairie::checksum_type,
    checksum        => $directmairie::download_checksum,
  }

  # Create directory for users uploaded pictures
  file { $picture_dir:
    ensure => directory,
    mode   => '0700',
    owner  => $system_username,
  }

  # Create systemd unit-file and notify service
  file { $_systemd_unit_file:
    ensure  => file,
    mode    => '0644',
    owner   => 0,
    group   => 0,
    content => epp('directmairie/directmairie.service.epp'),
    require => Archive['download-directmairie-jar'],
  }

  ~> service { 'directmairie':
    ensure    => $app_ensure,
    enable    => true,
    require   => [
      Package[$java_package],
      Postgresql::Server::Db[$db_name],
      File[$picture_dir],
    ],
    subscribe => Archive['download-directmairie-jar'],
    provider  => 'systemd',
  }
}
