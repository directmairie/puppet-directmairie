# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 7.0.0 - 2024-07-04

* allow puppetlabs-stdlib < 10.0.0 and drop Puppet 6 support and drop Ubuntu 18.04 support #61
* use PDK 3.2.0 and Ruby 3.2.0 #60
 
## 6.2.0 - 2023-04-11

 * java_package update should notify service  #57

## 6.1.0 - 2023-02-24

 * add ubuntu-22.04 as supported OS #55
 * use pdk 2.5.0 #54

## 6.0.0 - 2022-10-12

 * add "host" parameter  #52

## 5.2.0 - 2022-09-27

 * Missing @param tag for parameters in puppet strings format #17
 * some cleanup #50
 * add checksum on downloaded archive #38
 * donwloading jar does not trigger service #48
 * donwload puppet modules from Github instead of puppet forge (donwload limit is higer) #49
 * allow puppetlabs/postgresql < 9.0.0
 * add fact service_provider during unit tests #47

## 5.1.0 - 2022-04-20

 * add beaker acceptance tests for Ubuntu20.04 #45
 * add parameter permitting to enable TLS usage with SMTP #44

## 5.0.0 - 2022-04-19

 * README.md file use some parts of template, needs to be updated #26
 * add parameter mailfrom #41
 * maintenance pdk = 2.3.0, drop Puppet5 support #33
 * add Ubuntu20.04 as supported OS #15

## 4.2.0 - 2022-01-05

 * allow puppet-archive < 7.x, puppetlabs-postgresql < 8.x, puppetlabs-stdlib < 9.x #39
 * Change default value for download URL of DirectMairie (set to v2.1.0) #36
 * add puppet-strings gem 2.8.0 to Gemfile #37

## 4.1.0 - 2021-11-22

 * add parameter app_ensure #34

## 4.0.0 - 2020-11-22

 * function postgresql_password is deprecated #24
 * a database role is hard coded and not required #31
 * Optimize CI jobs with needs #30
 * usage default value download_url to use the official release package #29

## 3.0.0 - 2020-10-05

 * modify systemd unit file does not schedule refresh #2
 * Add acceptance tests , drop Ubntu16.04 and add Ubuntu18.04 support #11
 * some options of DirectMarie are renamed between 2.0 and probably next major version #13
 * why git is installed by default ? #14
 * Use a parameter like $download_url instead of $refspec and $ci_job_name #16
 * bump pdk version to 1.15.0 #18
 * title of some file resources are words #21
 * title of service resource use words instead of name of service #22
 * jar_name is local variable not a parameter #23
 * add Puppet 7 support, drop Puppet 4 #25
 * fix metadata.json in operatingsystem_support section #28

## 2.1.0 - 2020-02-06

### Changed

* Downloaded jar file from now includes refspec/version in its name (eg `directmairie-2.1.0.jar`)

## 2.0.0 - 2019-09-24

### Changed

* Rename module from `amies` to `directmairie` #9
* Remove `ccze` package #1

## 1.0.2 - 2019-06-26

### Added

* First release 
